extern crate rand;
extern crate byteorder;

use std::env;
use std::io::Read;
use std::io::Cursor;
use std::net::UdpSocket;
use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian};

fn main() {


    if env::args().count() != 2 {
        println!("Specify mumble server hostname:port");
        return;
    }

    let address = env::args().nth(1).unwrap();

    println!("{:?}", address);

    let socket = UdpSocket::bind("0.0.0.0:0").expect("Can't bind");
    socket.connect(address).expect("Can't connect");

    let packet_id = rand::random::<i64>();

    let mut out_buf = vec![];


    out_buf.write_i32::<BigEndian>(0).unwrap();
    out_buf.write_i64::<BigEndian>(packet_id).unwrap();



    socket.send(&out_buf);

    let mut buf = [0; 24];

    socket.recv_from(&mut buf).expect("Failed to read response");
    let mut reader = Cursor::new(buf);


    let mut version = [0; 4];
    reader.read(&mut version).expect("Failed to read version");
    let check_packet_id = reader.read_i64::<BigEndian>().expect(
        "Failed to read ident",
    );

    if packet_id != check_packet_id {
        println!("Wrong packet");
    }

    let connected_users = reader.read_u32::<BigEndian>().expect(
        "Failed to read connected users count",
    );

    let max_users = reader.read_u32::<BigEndian>().expect(
        "Failed to read max users count",
    );

    let bandwidth = reader.read_u32::<BigEndian>().expect(
        "Failed to read bandwidth",
    );


    println!(
        "Version {:?}\nUsers {:?}/{:?}\nBandwidth {:?}",
        version,
        connected_users,
        max_users,
        bandwidth
    );
}
